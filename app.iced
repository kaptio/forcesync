express = require('express')
path = require('path')
favicon = require('serve-favicon')
logger = require('morgan')
cookieParser = require('cookie-parser')
bodyParser = require('body-parser')
fs = require('fs')
passport = require('passport')
LocalStrategy = require('passport-local').Strategy
require('mongoose').connect(process.env.MONGOHQ_URL)
ConfigModel = require('./models/config')


loggedIn = (req, res, next) ->
  if req.user
    next()
  else
    res.redirect '/login'

app = express()

# view engine setup
app.set 'views', path.join(__dirname, 'views')
app.set 'view engine', 'jade'

app.use logger('dev')
app.use bodyParser.json()
app.use bodyParser.urlencoded(extended: false)
app.use cookieParser()

app.use require('express-session')(
  secret: 'v97ahoc7hao3ndp8v08a7e09a8na98en9nakdj'
  resave: false
  saveUninitialized: false)

app.use passport.initialize()
app.use passport.session()

app.use require('less-middleware')(path.join(__dirname, 'public'))
app.use express.static(path.join(__dirname, 'public'))
app.use '/vendor/', express.static(path.join(__dirname, 'bower_components'))

authRouter = express.Router()
require('./auth_router') authRouter
app.use '/', authRouter

router = express.Router()
router.use loggedIn
fs.readdirSync(path.join(__dirname, 'routes')).forEach (file) ->
  name = file.substr(0, file.indexOf('.'))
  require(path.join(__dirname, 'routes', name)) router
  return
app.use '/', router

# API stuff:
bodyParser = require('body-parser')
app.use bodyParser.json()
app.use bodyParser.urlencoded(extended: true)
apiRouter = express.Router()
apiRouter.use loggedIn
fs.readdirSync(path.join(__dirname, 'api')).forEach (file) ->
  name = file.substr(0, file.indexOf('.'))
  require(path.join(__dirname, 'api', name)) apiRouter
  return
app.use '/api/', apiRouter

# passport config
Account = require('./models/account')
passport.use new LocalStrategy(Account.authenticate())
passport.serializeUser Account.serializeUser()
passport.deserializeUser Account.deserializeUser()

# catch 404 and forward to error handler
app.use (req, res, next) ->
  err = new Error('Not Found')
  err.status = 404
  next err
  return

# error handlers
# development error handler
# will print stacktrace
if app.get('env') == 'development'
  app.use (err, req, res, next) ->
    res.status err.status or 500
    res.render 'error',
      message: err.message
      error: err
    return

# production error handler
# no stacktraces leaked to user
app.use (err, req, res, next) ->
  res.status err.status or 500
  res.render 'error',
    message: err.message
    error: {}
  return

GLOBAL.globalConfig = null
ConfigModel.findOne {}, (err, config) ->
  if err
    throw err
    return
  GLOBAL.globalConfig = config

  unless globalConfig
    GLOBAL.globalConfig = new ConfigModel()

  require './forcesyncd'

module.exports = app
