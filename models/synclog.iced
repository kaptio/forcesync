moment = require('moment')
_ = require('lodash')
mongoose = require('mongoose')
Schema = mongoose.Schema


syncLogSchema = new Schema
  user: {type: Schema.Types.ObjectId, ref: '_User', index: true}
  table: {type: String, index: true}
  timestamp: Date
  updated: Array
  deleted: Array

_.assign syncLogSchema.methods,
  getUTC: ->
    moment.utc(@timestamp).format()

module.exports = mongoose.model('_SyncLog', syncLogSchema, '_synclog')
