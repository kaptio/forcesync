yaml = require('js-yaml')
_ = require('lodash')
mongoose = require('mongoose')
Schema = mongoose.Schema


configSchema = new Schema
  tables: String
  defaultCron: String
, {_id: false}

_.assign configSchema.methods,
  getParsedTables: ->
    yaml.safeLoad(@tables)

  matchTables: (tables) ->
    reLst = []
    for t in @getParsedTables()
      # Check if table name is regex:
      if /^\/(.*)\/$/.test(t.table)
        # String is already valid js regex, so we just eval the string:
        reLst.push eval(t.table)
      else
        # Create regex out of non regex string, to be able to apply the same
        # filter as for a regex object:
        reLst.push new RegExp('^' + t.table + '$')
    tables.filter((t) -> reLst.some((re) -> re.test(t)))

  getTableParams: (subjectTable) ->
      params = null

      for t in @getParsedTables()
        if /^\/(.*)\/$/.test(t.table)
          re = eval(t.table)
        else
          re = new RegExp('^' + t.table + '$')
        if re.test(subjectTable)
          params = _.assign({}, t, {table: subjectTable})

      unless params
        params = {table: subjectTable}

      unless params.streaming?
        params.streaming = false

        unless params.cron?
          params.cron = @defaultCron
      else
        params.cron = false

      return params

module.exports = mongoose.model('_Config', configSchema, '_config')
