mongoose = require('mongoose')
Schema = mongoose.Schema
passportLocalMongoose = require('passport-local-mongoose')


accountSchema = new Schema
  username: {type: String, unique: true}
  password: String

accountSchema.plugin(passportLocalMongoose)

module.exports = mongoose.model('_Account', accountSchema, '_accounts')
