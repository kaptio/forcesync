Promise = require('bluebird')
moment = require('moment')
mongoose = require('mongoose')
jsforce = require('jsforce')
Inspect = require('../components/inspect')
_ = require('lodash')
Schema = mongoose.Schema


oauth2 = new jsforce.OAuth2
  clientId: process.env.SF_CLIENT_ID
  clientSecret: process.env.SF_CLIENT_SECRET

userSchema = new Schema
  id: {type: String, primary: true}
  username: {type: String, unique: true}
  organizationId: {type: String, index: true}
  accessToken: String
  accessTokenIssuedAt: Date
  refreshToken: String
  instanceUrl: String
  photo: String
  photoThumb: String
  tables: Array
, {_id: false}

_.assign userSchema.methods,
  getTableNames: ->
    @getTables().map((t) -> t.table)

  getTables: ->
    @tables.map (t) -> globalConfig.getTableParams(t)

  getForceConn: ->
    new jsforce.Connection
      instanceUrl: @instanceUrl
      accessToken: @accessToken

  refreshAccessToken: (cb) ->
    await oauth2.refreshToken @refreshToken, defer err, res
    if err
      cb(err)
      return
    @accessToken = res.access_token
    @instanceUrl = res.instance_url
    @accessTokenIssuedAt = Date(res.issued_at)
    @save (err) -> cb(err, res)

  autoRefreshAccessToken: -> new Promise (resolve, reject) =>
    # Refresh access token if it is more than 1 hour old:
    if moment(@accessTokenIssuedAt).isBefore(moment().subtract(1, 'hours'))
      @refreshAccessToken (err, res) ->
        if err
          reject(err)
        else
          resolve(res)
    else
      resolve()

  configure: -> new Promise (resolve, reject) =>
    inspect = new Inspect(@getForceConn())
    await inspect.getQueriableTableNames().catch(reject).then defer allTables

    configTables = globalConfig.matchTables(allTables)
    await inspect.getRelated(configTables).catch(reject).then defer tables

    @tables = tables

    await @save defer err
    if err
      reject(err)
    else
      resolve(this)

    return

# Update global config on saving config:
userSchema.post 'save', (config) ->
  globalConfig = config

# userSchema.pre 'remove', (next) ->
#   # Remove user's SyncLog
#   mongoose.model('_SyncLog').remove({user: this._id}).exec()
#   await mongoose.connection.db.collectionNames defer err, collectionNames
#   if err
#     console.warn(colors.yellow(err))

#   for cName in collectionNames
#     getModel()

#   next()

module.exports = mongoose.model('_User', userSchema, '_users')
