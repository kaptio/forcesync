S = require('string')
argv = require('yargs').argv
colors = require('colors')
read = require('read')
jsforce = require('jsforce')
Spinner = require('cli-spinner').Spinner
Table = require('cli-table')
_ = require('lodash')

Sync = require('./components/sync')
Inspect = require('./components/inspect')

models = require('./models')
User = require('./models/user')

mongoose = require('mongoose')
mongoose.connect(process.env.MONGOHQ_URL)

config = null

exit = ->
  mongoose.connection.close()

class ForceSync
  @_getCommands: ->
    Object.getOwnPropertyNames(ForceSync.prototype).sort().filter (p) ->
      typeof ForceSync.prototype[p] == 'function' and not S(p).startsWith('_')

  _getUserName: ->
    if argv.u?
      return argv.u
    else if argv.user?
      return argv.user
    else
      return argv._[1]

  _getUser: (cb) ->
    username = @_getUserName()

    await User.findOne {username: username}, defer err, user
    if err
      console.error colors.red(err)
      return
    unless user
      console.error colors.red("UNKNOWN USER: #{username}")

    cb.call(this, user)

  syncall: (argv, cb) ->
    await @_getUser defer user
    sync = new Sync(user)

    console.log 'FORCE SYNC GO! :D'.rainbow
    console.log '!!! 1 W1LL F0R53 5YNC Y0UR 5CULL !!!'.zalgo.red
    console.log '═════════════════════════════════════════'.rainbow

    await sync.syncAll defer()

    cb.call(this)

  checkconfig: (argv, cb) ->
    username = argv._[1] or argv.u

    await User.findOne {username: username}, defer err, user
    if err
      console.error colors.red(err)

    if argv.raw?
      console.log user
      exit()
      cb.call(this)
      return

    for k in ['username', 'userId', 'organizationId', 'accessToken', 'instanceUrl']
      console.log "#{k.grey}: #{user[k].white}"

    _maxCronLen = Math.max.apply(null, user.getTables().map((t)->S(t.cron).length))

    configTable = config.matchTables(user.getTableNames())

    tables = user.getTables().map (t) ->
      if t.cron
        prefix = S(t.cron).padRight(_maxCronLen).s.cyan
      else if t.streaming
        prefix = S('Streaming').padRight(_maxCronLen).s.rainbow
      else
        prefix = S('Unconfigured').padRight(_maxCronLen).s.red

      if t.table in configTable
        tname = t.table.green
      else
        prefix = prefix.dim
        tname = t.table.grey.dim

      return "■ #{prefix}  #{tname}"

    console.log 'tables:'.grey
    console.log tables.join('\n')

    cb.call(this)

  tablelist: (argv, cb) ->
    await @_getUser defer user

    inspect = new Inspect(user.getForceConn())
    await inspect.getQueriableTableNames defer names
    console.log names.join('\n')

    cb.call(this)

  describe: (argv, cb) ->
    await @_getUser defer user

    table = argv._[2] or argv._[1] or argv.t

    inspect = new Inspect(user.getForceConn())
    await inspect.describe table, defer meta
    console.log meta

    cb.call(this)

  relations: (argv, cb) ->
    username = argv._[1] or argv.u
    tables = argv._[2] or argv.t
    tables = "#{tables}".split(',')

    await sfConnection username, defer sf

    inspect = new Inspect(sf)
    await inspect.getRelated tables, defer relations
    console.log relations

    cb.call(this)

  configure: (argv, cb) ->
    await @_getUser defer user

    await user.configure().catch((err) -> throw err).then defer()

    # spinner = new Spinner('Getting queriable table names... %s'.cyan)
    # spinner.start()

    # inspect = new Inspect(sf)
    # await inspect.getQueriableTableNames defer allTables
    # spinner.stop()
    # console.log()  # new line

    # configTables = config.matchTables(allTables)

    # console.log 'Getting table relations...'.cyan

    # inspect.on 'describe', ->
    #   process.stdout.write('■')

    # await inspect.getRelated(configTables).then defer tables
    # console.log()  # new line

    # _table = new Table
    #   head: ['In', 'Out']
    #   colWidths: [32, 32]
    #   style:
    #     head: ['cyan']

    # _table.push([
    #   configTables.sort().map((t) -> t.green).join('\n')
    #   tables.sort().map((t) -> if t in configTables then t.green else t.grey.dim).join('\n')
    # ])

    # console.log(_table.toString())

    # await User.findOneAndUpdate {username: username}, {tables: tables}, defer err
    # if err
    #   console.error colors.red(err)
    #   exit()

    console.log 'USER CONFIG UPDATED!\n'.green

    cb.call(this)


forcesync = new ForceSync()

availCommands = ForceSync._getCommands()
if argv._[0] in availCommands
  await require('./models/config').findOne {}, defer err, config
  if err
    throw err

  GLOBAL.globalConfig = config

  forcesync[argv._[0]].call forcesync, argv, ->
    exit()
else
  console.error "Invalid command: \"#{argv._[0]}\"".red
  console.log 'Usage: '.cyan + 'forcesync <command> [user] [options]'.white
  console.log 'Available Commands:'.cyan
  console.log availCommands.map((c)->"● #{c}").join('\n').white
  exit()
