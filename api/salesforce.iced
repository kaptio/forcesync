passport = require('passport')
db = require('monk')(process.env.MONGOHQ_URL)
User = require('../models/user')
_ = require('lodash')


module.exports = (router) ->
  router.param 'table', (req, res, next, table) ->
    req.collection = db.get(table)
    next()

  router.get '/sf/:userId/:table', (req, res, next) ->
    await User.findOne {id: req.params.userId}, defer err, user
    if err
      return next(err)

    query = {_user: user._id}
    if req.query.q?
      try
        json = JSON.parse(req.query.q)
        query = _.assign({}, json, query)
      catch err
        return next(err)

    await req.collection.find query, {sort: {'_id': -1}}, defer err, docs
    if err
      return next(err)

    res.contentType 'application/json'
    res.send docs

  router.get '/sf/:userId/:table/:id', (req, res, next) ->
    await User.findOne {id: req.params.userId}, defer err, user
    if err
      return next(err)
    await req.collection.findOne {_user: user._id, id: req.params.id}, defer err, doc
    if err
      return next(err)

    res.contentType 'application/json'
    res.send doc
