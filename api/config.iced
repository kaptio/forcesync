ConfigModel = require('../models/config.iced')


module.exports = (router) ->
  router.get '/config/', (req, res, next) ->
    await ConfigModel.findOne {}, defer err, doc
    if err
      return next(err)
    res.send doc

  router.patch '/config/', (req, res, next) ->
    await ConfigModel.update {}, req.body, {upsert: true}, defer err
    if err
      return next(err)
    res.send {}
