passport = require('passport')
db = require('monk')(process.env.MONGOHQ_URL)

module.exports = (router) ->
  router.param 'cName', (req, res, next, cName) ->
    req.collection = db.get(cName)
    # res.contentType 'application/json'
    return next()

  router.get '/_raw/:cName', (req, res, next) ->
    await req.collection.find {}, {sort: {'_id': -1}}, defer err, docs
    if err
      return next(err)
    res.send docs

  router.post '/_raw/:cName', (req, res, next) ->
    await req.collection.insert req.body, {}, defer err, docs
    if err
      return next(err)
    res.send docs

  router.get '/_raw/:cName/:id', (req, res, next) ->
    await req.collection.findOne {id: req.params.id}, defer err, doc
    if err
      return next(err)
    res.send doc

  router.put '/_raw/:cName/:id', (req, res, next) ->
    options =
      safe: true
      multi: false
    await req.collection.update {id: req.params.id}, {$set: req.body}, options, defer err, doc
    if err
      return next(err)
    res.send if doc == 1 then msg: 'success' else msg: 'error'

  router.delete '/_raw/:cName/:id', (req, res, next) ->
    await req.collection.remove {id: req.params.id}, defer err, doc
    if err
      return next(err)
    res.send if doc == 1 then msg: 'success' else msg: 'error'
