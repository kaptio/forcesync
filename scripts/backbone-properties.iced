do (Backbone) ->
  _old = Backbone.Model::constructor

  defineProperty = (model, key) ->
    Object.defineProperty model, key,
      get: -> model.get key
      set: (value) -> model.set key, value

  Backbone.Model::constructor = ->
    modelInstance = this
    _old.apply(this, arguments)

    for key, val of @defaults
      defineProperty(this, key)

    for key, val of @properties
      unless _.isPlainObject(val)
        val = {get: val}
      Object.defineProperty(this, key, val)

    return
