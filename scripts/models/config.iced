yaml = require('js-yaml')

class Config extends Backbone.Model
  url: '/api/config/'

  initialize: ->
    App.socket.on "config:update", (data) => @set(data)

  sync: (method, model, options) ->
    # Method should be either "read or patch"
    _method = method
    unless _method == 'read'
      _method = 'patch'
    Backbone.sync.call(this, _method, model, options)

  defaults:
    __v: null
    _id: null
    tables: ''
    defaultCron: '*/10 * * * *'

  properties:
    tablesJson: ->
      try
        yaml.safeLoad(@tables)
      catch e
        e.message


module.exports = Config
