moment = require('moment')

class User extends Backbone.Model
  urlRoot: '/api/_raw/_users/'

  initialize: ->
    super
    App.socket.on "user/#{@id}:update", (data) => @set(data)

  defaults:
    username: String
    organizationId: String
    accessToken: String
    accessTokenIssuedAt: Date
    refreshToken: String
    instanceUrl: String
    photo: String
    photoThumb: String
    tables: []

  properties:
    issuedAt: ->
      moment(@accessTokenIssuedAt).format('[<i class="fa fa-calendar fa-fw"></i>]MMM D. [<i class="fa fa-clock-o fa-fw"></i>]HH:mm')

    query: ->
      _createRequest = (table, _query) =>
        $.ajax
          type: 'GET'
          url: "/api/sf/#{@id}/#{table}/"
          data: _query

      obj = {}
      @tables.forEach (table) =>
        Object.defineProperty obj, table,
          get: -> {find: (_query) -> _createRequest(table, _query)}
      return obj

class User.Collection extends Backbone.Collection
  model: User
  url: User::urlRoot


module.exports = User
