window.TPL = templates.templates
window.App = {}

require('./monkey')
require('./backbone-properties')

HomeView = require('./views/HomeView')
UsersView = require('./views/UsersView')
UserDetailView = require('./views/UserDetailView')
NavigationView = require('./views/NavigationView')
ConfigView = require('./views/ConfigView')

App.socket = io()
# Backbone.socket = App.socket
App.socket.on 'log', (data) -> console.log(data)

App.mainView = null


addTrailingSlash = ->
  # Make sure that path has a trailing slash.
  # This is important for navigation. i.e. we don't want a page to have
  # two possible urls.
  path = window.location.pathname
  unless path[path.length-1] == '/'
    # TODO: Add support for parameters and hash strings.
    window.history.replaceState null, document.title, path + '/'


window.addEventListener 'stateChange', addTrailingSlash
addTrailingSlash()


class Router extends Backbone.Router
  routes:
    '': 'home'
    'users/': 'users'
    'users/:id/': 'userDetail'
    'config/': 'config'
    'server-log/': 'serverLog'

  home: ->
    App.mainView = new HomeView()

  users: ->
    App.mainView = new UsersView()

  userDetail: (id) ->
    App.mainView = new UserDetailView().fetch(id)

  config: ->
    App.mainView = new ConfigView()

  serverLog: ->
    App.mainView = new ServerLogView()


App.router = new Router()
App.navigationView = new NavigationView()


Backbone.history.start({pushState: true})
