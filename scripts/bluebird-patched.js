// Iced coffeescript overwrites prototype.defer for all objects.

Promise = require('bluebird')
Promise._defer = Promise.defer

module.exports = Promise
