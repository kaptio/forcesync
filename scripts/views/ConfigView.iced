Config = require('../models/config.iced')
RactiveView = require('./RactiveView')

ConfigView = RactiveView.extend
  template: TPL.config

  data: ->
    loading: true

  oncomplete: ->
    @config = new Config()
    @config.fetch().then =>
      @config.set({})  # Just a hack to reset hasChanged() to false
      @set
        config: @config
        loading: false

    @config.on 'change:tables', => @update('config.tablesJson')

  onSave: (e) ->
    e.original.preventDefault()
    @set 'sending', true
    @config.save().then => @set 'sending', false


module.exports = ConfigView
