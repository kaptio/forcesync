autosize = require('autosize')


RactiveView = Ractive.extend
  el: '#application'

  decorators:
    autosize: (node) ->
      autosize(node)
      {teardown: -> autosize.destroy(node)}

  oninit: ->
    if @el == '#application'
      App.router.once 'navigate', @teardown, this

    for k, v of @__proto__
      if k.substr(0, 2) == 'on'
        eventName = k.substr(2)
        firstChr = eventName.substr(0, 1)
        if firstChr == firstChr.toUpperCase()
          @on firstChr.toLowerCase() + eventName.substr(1), this[k].bind(this)

module.exports = RactiveView
