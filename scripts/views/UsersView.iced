User = require('../models/user.iced')
RactiveView = require('./RactiveView')

UsersView = RactiveView.extend
  template: TPL.users

  data: ->
    loading: true

  oncomplete: ->
    @users = new User.Collection()
    @set 'users', @users
    @users.fetch().then =>
      @update 'users'
      @set 'loading', false

  onRmUser: (e) ->
    user = e.context
    if confirm("Are you sure you want to delete\n#{user.username}?")
      # FIXME: It seems like the ractive backbone adapter is messing with
      #        instance methods of Backbone Models. Quickfix:
      Backbone.Model::destroy.apply(user)

  onClickUser: (e) ->
    # No need to reload the whole page...
    e.original.preventDefault()
    App.router.navigate("/users/#{e.context.id}/", {trigger: true})

  onRefreshToken: (e) ->
    e.original.preventDefault()
    user = e.context
    App.socket.emit 'refreshAccessToken', user.id
    App.socket.once "user/#{user.id}:update", =>
      @update(e.keypath)

module.exports = UsersView
