RactiveView = require('./RactiveView')


NavigationView = RactiveView.extend
  el: '#navigation'
  template: TPL.navigation

  data: ->
    href: '/'
    path: window.location.pathname
    items: [
      {name: 'Status', href: '/', icon: 'fa-tachometer'}
      {name: 'Users', href: '/users/', icon: 'fa-users'}
      {name: 'Config', href: '/config/', icon: 'fa-wrench'}
      {name: 'Server Log', href: '/server-log/', icon: 'fa-terminal'}
    ]

  oninit: ->
    RactiveView::oninit.apply(this)

    App.router.on('route', @_onNavigate, this)

  _onNavigate: ->
    # The template needs to know the path to mark which
    # navigation item is active.
    @set 'path', window.location.pathname

  onClickItem: (e) ->
    # No need to reload the whole page...
    e.original.preventDefault()
    App.router.navigate(e.context.href, {trigger: true})

  onteardown: ->
    App.router.on('route', @_onNavigate, this)

module.exports = NavigationView
