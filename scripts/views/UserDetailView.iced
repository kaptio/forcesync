User = require('../models/user')
RactiveView = require('./RactiveView')


UserDetailView = RactiveView.extend
  template: TPL.user

  data: ->
    configuring: false
    loading: true

  fetch: (id) ->
    @user = new User({id: id})
    @user.fetch().then =>
      @set
        user: @user
        loading: false
    this

  onConfigure: (e) ->
    @set 'configuring', true
    App.socket.emit 'configureUser', @user.id
    App.socket.once "user/#{@user.id}:update", (data) =>
      @update 'user'
      @set 'configuring', false

  # onSyncAll: ->
  #   @user.syncAll()


module.exports = UserDetailView
