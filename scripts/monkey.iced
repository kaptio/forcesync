((history) ->
  pushState = history.pushState
  history.pushState = ->
    event = new CustomEvent('stateChange')
    state = pushState.apply(history, arguments)
    window.dispatchEvent(event)
    return state

  replaceState = history.replaceState
  history.replaceState = ->
    event = new CustomEvent('stateChange')
    state = replaceState.apply(history, arguments)
    window.dispatchEvent(event)
    return state
)(window.history)
