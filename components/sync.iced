Promise = require 'bluebird'
jsforce = require 'jsforce'
colors = require('colors')
_ = require('lodash')
moment = require('moment')

models = require('../models')
SyncLog = require('../models/synclog')


class Sync
  constructor: (user, sf=null) ->
    @user = user

    unless @sf
      @sf = new jsforce.Connection
        instanceUrl: user.instanceUrl
        accessToken: user.accessToken

  fetchTable: (table, ids) -> new Promise (resolve, reject) =>
    console.log "═════════════════════════════════════════"
    console.log "Syncing table: #{colors.cyan(table).underline}"

    searchParams = {}
    if ids?.length
      searchParams = _.object(ids.map((id) -> ['Id', id]))
    query = @sf.sobject(table).find(searchParams)

    query.on 'record', (record) =>
      _.assign({}, record, {_user: @user._id})

      models.get(table).findOneAndUpdate {
        Id: record.Id,
        _user: @user._id
      }, _.assign({}, record, {_user: @user._id}), {upsert: true}, (err, created) ->
        # FIXME!!!
        # Sometimes database connection gets closed before this gets a chance to run
        if err
          console.error colors.red(err)

      process.stdout.write('■')

    query.on 'end', (query) =>
      if query.totalFetched
        console.log ''  # newline
      console.log "Synced #{query.totalSize} of #{query.totalFetched} entries".green

      resolve()

    query.on 'error', (err) =>
      console.warn(err)
      reject(err)

    query.run({autoFetch: true, maxFetch: 99999999999})

  syncAll: (cb) ->
    for table in @user.getTableNames()
      await SyncLog.create {
        user: @user._id
        table: table
        timestamp: new Date()
        updated: []
        deleted: []
      }, defer err, logEntry

      await @fetchTable(table).then defer()

    cb.call(this)

  autoSyncTable: (table, options) -> new Promise (resolve, reject) =>
    unless _.isPlainObject(options)
      options = {}

    # Get the latest entry in syncLog:
    if options?.startDate
      startDate = lastSync.timestamp
    else
      await SyncLog.findOne({table: table, user: @user._id}).sort({$natural:-1}).exec defer err, lastSync
      if err
        console.error color.red(err)
        return reject(err)

    if options?.endDate
      endDate = options.endDate
    else
      endDate = new Date()

    endDateUTC = moment.utc(endDate).format()

    # The API requires a period, at least 1 minute long.
    #
    nowMinusOne = moment().subtract(1, 'minutes')
    if not lastSync || moment(startDate).isAfter(nowMinusOne)
      startDateUTC = nowMinusOne.utc().format()
    else
      startDateUTC = moment.utc(startDate).format()

    err = []
    await
      @sf.sobject(table).updated startDateUTC, endDateUTC, defer err[0], updated
      @sf.sobject(table).deleted startDateUTC, endDateUTC, defer err[1], deleted

    deletedIds = deleted?.deletedRecords.map((d)->d.id)

    if err.some(Boolean)
      console.error err.join('\n').red
      return reject(err)
    else
      await SyncLog.create {
        user: @user._id
        table: table
        timestamp: endDate
        updated: updated.ids
        deleted: deletedIds
      }, defer err

      if err
        console.error color.red(err)
        return reject(err)

      if deletedIds?.length
        await models.get(table).remove {_user: @user._id, Id: {$in: deletedIds}}, defer()

      if updated?.ids?.length
        await @fetchTable(table, updated.ids).then defer()

      return resolve
        updated: updated.ids
        deleted: deletedIds

module.exports = Sync
