colors = require('colors')
Promise = require('bluebird')
EventEmitter = require('events').EventEmitter


class ForceInspect extends EventEmitter
  constructor: (sf) ->
    super

    @sf = sf

  getTables: -> new Promise (resolve, reject) =>
    @sf.describeGlobal (err, res) =>
      if err
        reject(err)
      else
        resolve(res.sobjects)
      return

  getQueriableTableNames: -> new Promise (resolve, reject) =>
    @getTables().catch(reject).then (tables) =>
      names = tables.filter((t) -> t.queryable && t.replicateable).map (t) -> t.name
      resolve(names)

  getRelated: (tables, cb) -> new Promise (resolve, reject) =>
    _recursive = (table, cb, rel, err) =>
      _rel = rel || []
      if table in _rel
        cb.call(this, _rel)
        return

      # Get all fields of table:
      @emit('describe')
      await @sf.describe table, defer err, meta
      if err
        cb.call(this, null, null, null, err)
        return

      # * Make sure table has meta available
      # * Make sure table is quriable and replicatable
      unless meta && meta.queryable && meta.replicateable
        cb.call(this, _rel)
        return

      # Push the current table to _rel right away to avoid recursion:
      _rel.push(table)

      # Loop through fields with value(s) in referenceTo:
      for field in meta.fields.filter((f) -> f.referenceTo.length)
        # field.referenceTo is a list of tables so we loop through them,
        # but there is no need to check tables who have already been checked,
        # so we make sure they're not alread in _rel
        for tableName in field.referenceTo.filter((n) -> n not in _rel)
          await _recursive tableName, defer(_rel), _rel, err
          if err
            reject(err)
            return

      cb.call(this, _rel)

    rel = []
    for table in tables
      await _recursive(table, defer(rel), rel)
    resolve(rel)


module.exports = ForceInspect
