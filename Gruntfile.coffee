path = require('path')
fs = require('fs')
_bowerSourceMaps = null

module.exports = (grunt) ->
  grunt.initConfig
    ractiveparse:
      all:
        files: [{
          src: 'templates/**/*.html',
          dest: 'public/build/templates.js'
        }]

    browserify:
      all:
        files:
          'public/build/application.js': 'scripts/main.iced'
        options:
          transform: ['icsify']
          browserifyOptions:
            extensions: ['.iced']
            debug: true

    bower_concat:
      all:
        dest: 'public/build/lib.js'
        cssDest: 'public/build/lib.css'
        exclude: ['underscore']
        dependencies:
          'backbone': ['lodash', 'jquery']
          'ractive-adaptors-backbone': ['ractive', 'backbone']
          'autogrow-textarea': ['jquery']
        mainFiles:
          'ractive': ['ractive.runtime.js']
          'socketio-client': ['socket.io.js']
        bowerOptions:
          relative: false

    copy:
      bowerSourceMaps:
        flatten: true
        expand: true
        src: ['bower_components/**/*.map']
        dest: 'public/build/'
        # TODO: Find a better solution. We shouldn't need to copy all the source maps
        #       Optimally bower_concat would append the source maps as a string to
        #       the compiled js/css file
        # filter: (filepath) ->
        #   unless @_bowerSourceMaps?
        #     @_bowerSourceMaps = do ->
        #       lines = fs.readFileSync('public/build/lib.js').toString().split('\n')
        #       comments = lines.filter (line) -> line.indexOf('sourceMappingURL') != -1
        #       comments.map (comment) -> comment.split('=')[1]
        #   @_bowerSourceMaps.join('\n').indexOf(filepath) != -1

    uglify:
      dist:
        files:
          'public/build/application.js': ['public/build/application.js']
        options:
          mangle:
            except: ['jQuery', 'Backbone', 'App']
          sourceMap: true

    less:
      all:
        files:
          "public/build/style.css": "public/stylesheets/**/*.less"

    express:
      all:
        options:
          script: './bin/www'
          harmony: true
          debug: true

    watch:
      clientSource:
        files: [
          'scripts/**/*.js'
          'scripts/**/*.coffee'
          'scripts/**/*.iced'
        ]
        tasks: ['browserify']

      ractiveTemplates:
        files: [
          'templates/**/*.html'
        ]
        tasks: ['ractiveparse']

      express:
        files: [
          'routes/**/*.js'
          'routes/**/*.coffee'
          'routes/**/*.iced'
          'views/**/*.jade'
          '!scripts/**/*.js'
          '!scripts/**/*.coffee'
          '!scripts/**/*.iced'
        ]
        tasks: ['express:dev']
        options:
          spawn: false

      less:
        files: [
          "public/stylesheets/**/*.less"
        ]
        tasks: ['less']


  grunt.loadNpmTasks('grunt-ractive-parse')
  grunt.loadNpmTasks('grunt-browserify')
  grunt.loadNpmTasks('grunt-express-server')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-less')
  grunt.loadNpmTasks('grunt-bower-concat')
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-contrib-copy')

  grunt.registerTask('build', ['bower_concat', 'copy:bowerSourceMaps', 'browserify', 'less', 'ractiveparse'])
  grunt.registerTask('dev', ['build', 'express', 'watch'])
  grunt.registerTask('production', ['build', 'uglify:dist'])
