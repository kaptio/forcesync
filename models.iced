mongoose = require('mongoose')
Schema = mongoose.Schema

x = {}

getModel = (table) ->
  schema = new mongoose.Schema {
    _user: {type: mongoose.Schema.Types.ObjectId, ref: '_User', index: true}
    Id: {type: String, index: true}
  }, {strict: false}

  unless x[table]?
    x[table] = mongoose.model(table, schema, table)
  return x[table]

x.get = getModel

module.exports = x
