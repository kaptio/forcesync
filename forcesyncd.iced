CronJob = require('cron').CronJob
S = require('string')
colors = require('colors')
_ = require('lodash')
moment = require('moment')

Sync = require('./components/sync')
User = require('./models/user')


_userData = {}

_setupCron = ->
  await User.find defer err, users

  if err
    console.error colors.red(err)
    return

  for user in users
    user.autoRefreshAccessToken().catch(console.error).then (res) =>
      if res
        console.log "New Access Token fetched for #{user.username}"

      unless _userData[user.id]?
        _userData[user.id] =
          sync: new Sync(user)
          jobs: {}
          tables: []

      # Shortcut:
      data = _userData[user.id]

      # Stringify each obj in array
      # It is not possible to compare objects
      currentTables = data.tables.map (t) -> JSON.stringify(t)
      newTables = user.getTables().map (t) -> JSON.stringify(t)

      # Compare JSON strings and parse to native js
      removed = _.difference(currentTables, newTables).map (s) -> JSON.parse(s)
      removedNames = removed.map (t) -> t.table
      added = _.difference(newTables, currentTables).map (s) -> JSON.parse(s)
      addedNames = added.map (t) -> t.table

      if added.length or removed.length
        console.log user.username

      # --- Removed ---
      # Jobs:
      for table in removed
        # Stop and remove job for table:
        if data.jobs[table.table]?
          data.jobs[table.table].stop()
          delete data.jobs[table.table]

      # Update _userData:
      data.tables = data.tables.filter (t) -> t.table not in removedNames

      # --- Added ---
      for table in added
        # Update _userData:
        data.tables.push table

        if table.cron
          console.log "#{table.cron.cyan} #{table.table}"

          job = new CronJob
            cronTime: table.cron
            start: true
            context:
              sync: data.sync
              table: table
            onTick: ->
              @sync.autoSyncTable(@table.table).then (res) =>
                updated = colors.green(res.updated.length)
                deleted = colors.red(res.deleted.length)
                dt = moment().format()
                console.log "┏ #{dt.grey}: #{@table.table.white}"
                console.log '┗' + S('━').repeat(S(dt).length + 2).s + " Updated: #{updated} ━ Deleted: #{deleted}"

          data.jobs[table.table] = job

        if table.streaming
          console.log 'Streaming'.rainbow + " #{table.table} " + '(Streaming not implemented)'.yellow


_setupCron()
setInterval(_setupCron, 5000)
