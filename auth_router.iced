express = require('express')
passport = require('passport')
Account = require('./models/account')

module.exports = (router) ->
  # router.post '/register/', (req, res) ->
  #   account = new Account(username: req.body.username)
  #   await Account.register account, req.body.password, defer err, account
  #   if err
  #     return res.render('register', account: account)
  #   passport.authenticate('local') req, res, ->
  #     res.redirect '/'

  router.get '/login/', (req, res) ->
    res.render 'login', user: req.user

  router.post '/login', passport.authenticate 'local',
    successRedirect: '/'
    failureRedirect: '/login'

  router.get '/logout/', (req, res) ->
    req.logout()
    res.redirect '/'