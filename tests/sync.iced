global.unittest = true

colors = require('colors')
jsforce = require('jsforce')
_ = require('lodash')
moment = require('moment')

settings = require('../settings')
Sync = require('../components/sync')

models = require('../models')
User = models.User
SyncLog = models.SyncLog
db = models.db

process.on 'uncaughtException', (err) -> console.error(err.stack)


module.exports =
  setUp: (callback) ->
    # Clear the test database before we start:
    await User.remove {}, defer()

    # Create a connection to SalesForce:
    sf = new jsforce.Connection()
    await sf.login settings.testuser.username,
                   settings.testuser.password,
                   defer err, userInfo

    if err
      console.error(colors.red(err))
      process.exit(1)

    # Create a test user:
    await User.create {
      username: settings.testuser.username
      accessToken: sf.accessToken
      instanceUrl: sf.instanceUrl
      userId: userInfo.id
      organizationId: userInfo.organizationId
      tables: ['Contact']
    }, defer err, @user

    if err
      console.error(colors.red(err))
      process.exit(1)

    # Initiate a new Sync instance:
    @sync = new Sync(@user)

    callback()

  tearDown: (callback) ->
    # Clear the test database for good measure:
    await User.remove {}, defer()

    # Close the test database connection:
    #db.close()

    callback()

  syncAll: (test) ->
    test.expect 0
    await @sync.syncAll defer()
    test.done()

  autoSyncTable: (test) ->
    test.expect 8

    testName = 'ForceSync Test Contact'

    await @sync.sf.sobject('Contact').create {FirstName: testName, LastName: 'Zalgo'}, defer err, created
    console.log "CREATED".green + " #{testName} (#{created.id.grey})".white

    await SyncLog.create {
      user: @user._id
      table: 'Contact'
      timestamp: moment().utc().subtract(1, 'minutes').format()
    }, defer err, logEntry

    if err || !created?.success
      console.error colors.red(err)
      console.log colors.blue(created)

    test.ifError err
    test.ok created?.success

    syncOptions =
      endDate: moment().add(1, 'minutes').toDate()

    await @sync.autoSyncTable('Contact', syncOptions).then defer syncRes

    test.ok created.id in syncRes.updated

    await models.get('Contact').find {Id: created.id}, defer err, inMongo
    test.ifError err

    test.equal inMongo.length, 1

    # Cleanup:
    await @sync.sf.sobject('Contact').find({FirstName: testName}, 'Id').execute defer err, contacts

    if err
      console.error colors.red err
      console.info colors.blue contacts

    for id in contacts.map((c)->c.Id)
      await @sync.sf.sobject('Contact').del id, defer err, ret
      if err
        console.error colors.red err
        console.info colors.blue ret
      else
        console.log "DELETED".red + " #{testName} (#{ret.id.grey})".white

    # Test Auto Delete:
    await @sync.autoSyncTable('Contact', syncOptions).then defer syncRes

    test.ok created.id in syncRes.deleted

    await models.get('Contact').find {Id: created.id}, defer err, inMongo
    test.ifError err

    test.equal inMongo.length, 0

    test.done()
