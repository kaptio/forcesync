ForceSync
==========

### Setup:

    git clone git@bitbucket.org:kaptio/forcesync.git
    cd forcesync
    npm install

### Commands:

    # Add user to mongodb
    ./bin/forcesync adduser

    # List all users in mongodb
    ./bin/forcesync listusers

    # Check if user is authenticated with SalesForce
    ./bin/forcesync -u <user> checkuser

    # Get user's list of tables from SalesForce
    ./bin/forcesync -u <user> tablelist

    # Describe SalesForce table
    ./bin/forcesync -u <user> describe <table>

    # Get all tables related to input tables
    ./bin/forcesync -u <user> relations <table1[,table2,[table(n...)]>

    # Configure user. Figure out table relations and save in mongodb
    ./bin/forcesync -u <user> configure
