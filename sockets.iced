User = require './models/user'


module.exports = (io) ->
  io.on 'connection', (socket) ->
    socket.on 'refreshAccessToken', (id) ->
      await User.findOne {id: id}, defer err, user
      if err
        socket.emit 'log', err
        return

      await user.refreshAccessToken defer err, res
      if err
        socket.emit 'log', err
        return

      socket.emit 'log', res
      io.emit "user/#{user.id}:update", user

    socket.on 'configureUser', (id) ->
      await User.findOne {id: id}, defer err, user
      if err
        socket.emit 'log', err
        return

      user.configure().then ->
        io.emit "user/#{user.id}:update", user
      .catch (err) ->
        socket.emit 'log', err
