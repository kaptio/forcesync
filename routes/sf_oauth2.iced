jsforce = require('jsforce')
passport = require('passport')
User = require('../models/user')

callbackPath = '/oauth2/callback/'

oauth2 = new jsforce.OAuth2
  clientId: process.env.SF_CLIENT_ID
  clientSecret: process.env.SF_CLIENT_SECRET
  redirectUri: process.env.FORCESYNC_BASE_URL + callbackPath


module.exports = (router) ->
  #
  # Get authz url and redirect to it.
  #
  router.get '/oauth2/auth/', (req, res) ->
    res.redirect(oauth2.getAuthorizationUrl({scope:'full refresh_token'}))

  #
  # Pass received authz code and get access token
  #
  router.get callbackPath, (req, res) ->
    conn = new jsforce.Connection {oauth2: oauth2}
    code = req.param('code')

    await conn.authorize code, defer err, userInfo
    if err
      res.end(JSON.stringify(err))
      return

    await conn.sobject 'User'
    .find {Id: userInfo.id}, ['Username', 'FullPhotoUrl', 'SmallPhotoUrl']
    .execute defer err, records
    if err
      res.end(JSON.stringify(err))
      return

    userData = records[0]

    await User.update {id: userInfo.id}, {
      id: userInfo.id
      username: userData.Username
      organizationId: userInfo.organizationId
      accessToken: conn.accessToken
      accessTokenIssuesAt: new Date()
      refreshToken: conn.refreshToken
      instanceUrl: conn.instanceUrl
      photo: userData.FullPhotoUrl
      photoThumb: userData.SmallPhotoUrl
    }, {upsert: true}, defer err

    if err
      res.end(JSON.stringify(err))
      return

    res.redirect('/users/')
