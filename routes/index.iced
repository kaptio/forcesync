passport = require('passport')

module.exports = (router) ->
  router.get '/', (req, res, next) ->
    res.render 'index', {title: 'ForceSync'}

  router.get '/users/', (req, res, next) ->
    res.render 'index', {title: 'Users - ForceSync'}

  router.get '/users/:id/', (req, res, next) ->
    res.render 'index', {title: 'Users - ForceSync'}

  router.get '/config/', (req, res, next) ->
    res.render 'index', {title: 'Config - ForceSync'}
